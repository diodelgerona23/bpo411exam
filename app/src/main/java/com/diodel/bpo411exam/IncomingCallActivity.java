package com.diodel.bpo411exam;

import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

public class IncomingCallActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnAnswer, btnDecline;
    ITelephony telephonyService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_incoming_call);
        btnAnswer = (Button) findViewById(R.id.btnAnswer);
        btnAnswer.setOnClickListener(this);
        btnDecline = (Button) findViewById(R.id.btnDecline);
        btnDecline.setOnClickListener(this);

//        try {
//
//
//            // TODO Auto-generated method stub
//            super.onCreate(savedInstanceState);
//
//            getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//            getWindow().addFlags(
//                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
//
//            setContentView(R.layout.activity_incoming_call);
//
//        } catch (Exception e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAnswer:
                closeApplication(v);
                break;
            case R.id.btnDecline:
                closeApplication(v);
                endCall();
                break;

        }
    }

    public void closeApplication(View view) {
        finish();
        moveTaskToBack(true);
    }

    //
    public void endCall() {
        try {


            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            try {

                Class c = Class.forName(tm.getClass().getName());

                Method m = c.getDeclaredMethod("getITelephony");
                m.setAccessible(true);
                telephonyService = (ITelephony) m.invoke(tm);

                telephonyService.endCall();

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Exception", e.getMessage());
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
