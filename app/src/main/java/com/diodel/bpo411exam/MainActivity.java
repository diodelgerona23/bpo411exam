package com.diodel.bpo411exam;

import android.Manifest;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PERMISSION_REQUEST_READ_PHONE_STATE = 100;
    private TextView tvPhoneNumber;
    private Button btnBlockNumber, btnChangeCaller;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pref = getApplicationContext().getSharedPreferences("Pref411", 0); // 0 - for private mode
        editor = pref.edit();

        tvPhoneNumber = (TextView) findViewById(R.id.tv_phone_number);
        btnBlockNumber = (Button) findViewById(R.id.btn_block_number);
        btnBlockNumber.setOnClickListener(this);
        btnChangeCaller = (Button) findViewById(R.id.btn_caller_change);
        btnChangeCaller.setOnClickListener(this);

    }

    private void checkPermission() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE};
                requestPermissions(permissions, PERMISSION_REQUEST_READ_PHONE_STATE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        checkPermission();
        switch (v.getId()) {
            case R.id.btn_block_number:
                btnBlockNumber.setBackgroundColor(ContextCompat.getColor(this, R.color.button_color));
                btnChangeCaller.setBackgroundColor(ContextCompat.getColor(this, R.color.un_selected));
                editor.putString("event_type","block");
                editor.commit();
                break;
            case R.id.btn_caller_change:

                btnBlockNumber.setBackgroundColor(ContextCompat.getColor(this, R.color.un_selected));
                btnChangeCaller.setBackgroundColor(ContextCompat.getColor(this, R.color.button_color));
                editor.putString("event_type","change");
                editor.commit();

                break;

        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_READ_PHONE_STATE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission granted: " + PERMISSION_REQUEST_READ_PHONE_STATE, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permission NOT granted: " + PERMISSION_REQUEST_READ_PHONE_STATE, Toast.LENGTH_SHORT).show();
                }

                return;
            }
        }
    }

    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void broadcastReceive(Message event) {
        Log.e("broadcastReceive","broadcastReceive");
        showAlert(event);
    }

    private void showAlert(Message message) {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message.data);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

//    private String alternatingCharacters(int N) {
//        StringBuilder seriesOfChar = new StringBuilder();
//        for (int i = 0; i < N; i++) {
//            if (i % 2 == 0) {
//                seriesOfChar.append("+");
//            } else {
//                seriesOfChar.append("-");
//
//            }
//        }
//        return seriesOfChar.toString();
//    }
//
//    private List<Integer> sumOOfArray(int N) {
//        int total = 0, temp = 0, max = 5, min = -5;
//        Random random = new Random();
//        List<Integer> integerList = new ArrayList<>();
//        for (int i = 0; i < N; i++) {
//            int number = 0;
//            boolean isDuplicate = true;
//            do {
//                number = random.nextInt(max - min) + min;
//
//                if (!integerList.contains(number)) {
//                    if (i == (N - 1)) {
//                        if (!integerList.contains(total + number))
//                            isDuplicate = false;
//                    } else {
//                        isDuplicate = false;
//                    }
//
//                }
//            } while (isDuplicate);
//
//            if (i == (N - 1)) {
//                temp = (total < 0 ? total : -total);
//                integerList.add(temp);
//            } else {
//                total += number;
//                integerList.add(number);
//            }
//        }
//        return integerList;
//    }
}
