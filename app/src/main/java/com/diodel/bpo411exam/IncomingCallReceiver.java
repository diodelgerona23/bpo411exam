package com.diodel.bpo411exam;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Method;

public class IncomingCallReceiver extends BroadcastReceiver {
    ITelephony telephonyService;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        try {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            SharedPreferences prefs = context.getSharedPreferences("Pref411", 0);

            if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING)) {
                TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                try {

                    Class c = Class.forName(tm.getClass().getName());

                    Method m = c.getDeclaredMethod("getITelephony");
                    m.setAccessible(true);
                    telephonyService = (ITelephony) m.invoke(tm);

                    if ((number != null)) {
                        Log.e("IncomingCallReceiver", "number not null" + prefs.getString("event_type", ""));


                        if (number.equalsIgnoreCase(context.getString(R.string.phone_number))) {
                            if (prefs.getString("event_type", "").equalsIgnoreCase("change")) {
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        Intent i = new Intent(context, IncomingCallActivity.class);
                                        i.putExtras(intent);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        context.startActivity(i);
                                    }
                                }, 2000);
                            } else {
                                telephonyService.endCall();
                                Log.e("IncomingCallReceiver", "Ending the call from: " + number);
                                EventBus.getDefault().post(new Message(number + " is blocked"));
                            }

                        } else {
                            Log.e("IncomingCallReceiver", "this number is not blocked: " + number);

                        }


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Exception", e.getMessage());
                }

                Toast.makeText(context, "Ring " + number, Toast.LENGTH_SHORT).show();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}